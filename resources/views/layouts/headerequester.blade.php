<!DOCTYPE html>
<!--
Template Name: Wavefire
Author: <a href="https://www.os-templates.com/">OS Templates</a>
Author URI: https://www.os-templates.com/
Copyright: OS-Templates.com
Licence: Free to use under our free template licence terms
Licence URI: https://www.os-templates.com/template-terms
-->
<html lang="">
<!-- To declare your language - read more here: https://www.w3.org/International/questions/qa-html-language-declarations -->
<head>
<title>KatalogServiceBus</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="{{asset('styles/layout.css')}}" rel="stylesheet" type="text/css" media="all">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
</head>
<body id="top">
<div class="wrapper row0">
  <header id="header" class="hoc clear"> 
    <div id="logo" class="one_quarter first">
      
      <h1><a href="index.html"><span>K</span>atalog<span>S</span>ervice<span>B</span>us</a></h1>
    </div>
       
    <div class="menu">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <a id="navbarDropdown" class="nav-link" href="/requesters/detailRequester/{{ Auth::user()->id }}" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
            {{ Auth::user()->nama_lengkap }} <span class="caret"></span>
        </a>&nbsp;&nbsp;<i class="fas fa-user"></i>
          &nbsp;||&nbsp;
          <a class="nav-link" href="{{ route('logout') }}"
                 onclick="event.preventDefault();
                               document.getElementById('logout-form').submit();">
                  {{ __('Logout') }}
          </a>&nbsp;&nbsp;<i class=" fas fa-sign-out-alt"></i>
  
              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  @csrf
              </form>
          
      </li>
      </ul>
      </div>
      <h5>Modul Requester</h5>
  </header>
</div>
<div class="wrapper row1">
  <section class="hoc clear"> 
    <nav id="mainav">
      <ul class="clear">
        <li><a href="{{ url('requesters/index') }}">Beranda</a></li>
        <li><a href="{{ url('requesters/layanantersediar') }}">Layanan Tersedia</a></li>
        <li><a href="{{ url('requesters/layanandiajukanr') }}">Layanan Diajukan</a></li>
        <li><a href="{{ url('requesters/create') }}">Ajukan Layanan</a></li>
      </ul>
    </nav>
    {{-- <div id="searchform">
      <div>
        <form action="#" method="post">
          <fieldset>
            <legend>Pencarian Layanan</legend>
            <input type="text" placeholder="ketikan nama layanan&hellip;">
            <button type="submit"><i class="fas fa-search"></i></button>
          </fieldset>
        </form>
      </div>
    </div> --}}
  </section>
</div>
<main class="py-4">
    @yield('content')
</main>

<!-- JAVASCRIPTS -->
<script src="scripts/jquery.min.js"></script>
<script src="scripts/jquery.backtotop.js"></script>
<script src="scripts/jquery.mobilemenu.js"></script>
</body>
</html>