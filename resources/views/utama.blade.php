<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>KatalogServiceBus</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
        <style>
            html, body {
                background-color: #f4f4f4;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 60px;
                font-family: Georgia, 'Times New Roman', Times, serif;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-top: -180px;
            }

            .card {
                text-align: center;
                margin-left: 100px;
                width: 20rem;
                margin-top: 30px;
            }

            .card-header {
            color: #000000;
            text-align: left;
            background-color: #636b6f;
            font-family: Georgia, 'Times New Roman', Times, serif;
            }

            .list-group-item{
                background-color: #f4f4f4;
                
            }
            
            a {
            color:#636b6f;
            text-decoration: none;
            background-color:transparent;
            font-family: Georgia, 'Times New Roman', Times, serif;
            }

            .a:hover {
            color: #000000;
            text-decoration: underline;
            background-color: transparent;
            }

            li:hover {
            color: #000000;
            text-decoration: underline;
            background-color: #95103B;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            {{-- @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif --}}

            <div class="content">
                <div class="title m-b-md">
                    <span style="color:#95103B;">K</span>atalog <span style="color:#95103B;">S</span>ervice <span style="color:#95103B;">B</span>us
                   
                </div>
            
                <div class="card">
                    <div class="card-header">
                        Anda Login Sebagai ...
                      </div>
                      <ul class="list-group list-group-hover">
                        <li class="list-group-item"><a href="login" class="btn">Admin</a></li>
                        <li class="list-group-item"><a href="login" class="btn">Admin Katalog</a></li>
                        <li class="list-group-item"><a href="login" class="btn">Requester</a></li>
                      </ul>
                </div>

                {{-- <div class="links">
                    <a href="https://laravel.com/docs">Docs</a>
                    <a href="https://laracasts.com">Laracasts</a>
                    <a href="https://laravel-news.com">News</a>
                    <a href="https://blog.laravel.com">Blog</a>
                    <a href="https://nova.laravel.com">Nova</a>
                    <a href="https://forge.laravel.com">Forge</a>
                    <a href="https://vapor.laravel.com">Vapor</a>
                    <a href="https://github.com/laravel/laravel">GitHub</a>
                </div> --}}
            </div>
        </div>
    </body>
</html>
