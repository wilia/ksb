
  @extends('layouts.app')

  @section('content')
    <div class="py-4 d-flex justify-content-end align-items-center">
      <h2 class="mr-auto">Data Mahasiswa</h2>
      <a href="{{route('mahasiswas.create')}}" class="btn btn-primary">
        Tambah Data Mahasiswa
      </a>
    </div>
    @if (session()->has('pesan'))
        <div class="alert alert-success">
          {{session()->get('pesan')}}
        </div>
    @endif
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">NIM</th>
                <th scope="col">Nama</th>
                <th scope="col">Tempat Lahir</th>
                <th scope="col">Tanggal Lahir</th>
                <th scope="col">Fakultas</th>
                <th scope="col">Jurusan</th>
                <th scope="col">IPK</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($mahasiswas as $mahasiswa)
            <tr>
                <th>{{$loop->iteration}}</th>
                <th><a href="{{route('mahasiswas.detail',['mahasiswa'=>$mahasiswa->id])}}">
                  {{$mahasiswa->nim}}</a></th>
                <th>{{$mahasiswa->nama}}</th>
                <th>{{$mahasiswa->tempat_lahir}}</th>
                <th>{{$mahasiswa->tanggal_lahir}}</th>
                <th>{{$mahasiswa->fakultas}}</th>
                <th>{{$mahasiswa->jurusan}}</th>
                <th>{{$mahasiswa->ipk}}</th>
            </tr>
                
            @empty
                <td colspan="4" class="text-center">Data Barang Tidak Ada...</td>
            @endforelse
        </tbody>
    </table>
 
  @endsection
