<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <title>Detail Mahasiswa</title>
  </head>
  @extends('layouts.app')

  @section('content')
    <div class="py-4 d-flex justify-content-end align-items-center">
        <h2 class="mr-auto">Data {{$mahasiswa->nama}}</h2>
        <a href="{{route('mahasiswas.index')}}" class="btn btn-success mr-3">Data Mahasiswa</a>
        <a href="{{route('mahasiswas.edit', ['mahasiswa'=>$mahasiswa->id])}}" class="btn btn-primary">Edit</a>
        <form action="{{route('mahasiswas.delete',['mahasiswa' => $mahasiswa->id])}}"
            method="POST">
            @method('DELETE')
            @csrf
            <button type="submit" class="btn btn-danger ml-3">Hapus</button> 
        </form>
      </div>
      @if (session()->has('pesan'))
      <div class="alert alert-success">
        {{session()->get('pesan')}}
      </div>
  @endif
    <ul>
        <li>NIM : {{$mahasiswa->nim}}</li>
        <li>Nama Mahasiswa : {{$mahasiswa->nama}}</li>
        <li>Tempat Lahir : {{$mahasiswa->tempat_lahir}}</li>
        <li>Tanggal Lahir : {{$mahasiswa->tanggal_lahir}}</li>
        <li>Fakultas : {{$mahasiswa->fakultas}}</li>
        <li>Jurusan : {{$mahasiswa->jurusan}}</li>
        <li>IPK : {{$mahasiswa->ipk}}</li>
        <li>Diinput pada : {{$mahasiswa->created_at}}</li>
        <li>Diupdate pada : {{$mahasiswa->updated_at}}</li>
    </ul>
    

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
  
  @endsection
</html>