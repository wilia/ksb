<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <title>Form Mahasiswa</title>
  </head>
  @extends('layouts.app')

  @section('content')
    <h1>Edit Data Mahasiswa</h1>
    <form action="{{route('mahasiswas.update',['mahasiswa'=>$mahasiswa->id])}}" method="POST">
        @method('PUT')
        @csrf
        <div class="form-group">
            <label for='nim'>NIM</label>
            <div>
                <input type="text" class="form-control
                     @error('nim') is-valid @enderror"
                    id="nim" name="nim" value="{{old('nim')??$mahasiswa->nim}}">
                @error('nim')
                    <div class="text-danger">{{$message}}</div>
                @enderror
            </div>
        </div>
        <div class="form-group">
            <label for='nama_barang'>Nama Mahasiswa</label>
            <div>
                <input type="text" class="form-control
                     @error('nama') is-valid @enderror"
                    id="nama" name="nama" value="{{old('nama')??$mahasiswa->nama}}">
                @error('nama')
                    <div class="text-danger">{{$message}}</div>
                @enderror
            </div>
        </div>
        <div class="form-group">
            <label for='tempat_lahir'>Tempat Lahir</label>
            <div>
                <input type="text" class="form-control
                     @error('tempat_lahir') is-valid @enderror"
                    id="tempat_lahir" name="tempat_lahir" value="{{old('tempat_lahir')??$mahasiswa->tempat_lahir}}">
                @error('tempat_lahir')
                    <div class="text-danger">{{$message}}</div>
                @enderror
            </div>
        </div>
        <div class="form-group">
            <label for='tanggal_lahir'>Tanggal Lahir</label>
            <div>
                <input type="date" class="form-control
                     @error('tanggal_lahir') is-valid @enderror"
                    id="tanggal_lahir" name="tanggal_lahir" value="{{old('tanggal_lahir')??$mahasiswa->tanggal_lahir}}">
                @error('tanggal_lahir')
                    <div class="text-danger">{{$message}}</div>
                @enderror
            </div>
        </div>
        <div class="form-group">
            <label for='fakultas'>Fakultas</label>
            <div>
                <input type="text" class="form-control
                     @error('fakultas') is-valid @enderror"
                    id="fakultas" name="fakultas" value="{{old('fakultas')??$mahasiswa->fakultas}}">
                @error('fakultas')
                    <div class="text-danger">{{$message}}</div>
                @enderror
            </div>
        </div>
        <div class="form-group">
            <label for='jurusan'>Jurusan</label>
            <div>
                <input type="text" class="form-control
                     @error('jurusan') is-valid @enderror"
                    id="jurusan" name="jurusan" value="{{old('jurusan')??$mahasiswa->jurusan}}">
                @error('jurusan')
                    <div class="text-danger">{{$message}}</div>
                @enderror
            </div>
        </div>
        <div class="form-group">
            <label for='ipk'>IPK</label>
            <div>
                <input type="decimal" class="form-control
                     @error('ipk') is-valid @enderror"
                    id="ipk" name="ipk" value="{{old('ipk')??$mahasiswa->ipk}}">
                @error('ipk')
                    <div class="text-danger">{{$message}}</div>
                @enderror
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
    </form>
    

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>

  @endsection
</html>