@extends('layouts.headerequester')

@section('content')
<main class="hoc container clear"> 
  <h1>Form Pengajuan Layanan</h1>
  <form action="{{route('ksb_pengajuan_layanans.store')}}" method="POST">
    <div class="form-row">
      <div class="form-group col-md-6">
        <label for="nama_pemesan">Nama Pemesan</label>
        <input type="nama_pemesan" class="form-control" id="nama_pemesan" value="{{old('nama_pemesan')}}">>
      </div>
    </div>
    <div class="form-row">
      <div class="form-group col-md-6">
        <label for="instansi">Instansi</label>
        <input type="instansi" class="form-control" id="instansi" value="{{old('instansi')}}">>
      </div>
    </div>
    <div class="form-row">
    <div class="form-group col-md-6">
      <label for="nama_layanan">Nama Layanan</label>
      <input type="nama_layanan" class="form-control" id="nama_layanan" value="{{old('nama_layanan')}}">>
    </div>
    </div>
    <div class="form-row">
    <div class="form-group col-md-6">
      <label for="alamat_ip">Alamat IP</label>
      <input type="alamat_ip" class="form-control" id="alamat_ip" value="{{old('alamat_ip')}}">>
    </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-6">
          <label for="acces_ke">Accyes Key</label>
          <input type="acces_key" class="form-control" id="acces_key" value="{{old('acces_key')}}">>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-6">
          <label for="keterangan">Keterangan</label>
          <input type="keterangan" class="form-control" id="keterangan" value="{{old('keterangan')}}">>
        </div>
    </div>
    <button type="reset" class="btn btn-danger">Batal</button>
    <button type="submit" class="btn btn-primary">OK</button>
  </form>
</main>
@endsection