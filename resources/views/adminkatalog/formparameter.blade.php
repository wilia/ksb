@extends('layouts.header')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <table>
                            <thead>
                              <tr>
                                <th>No</th>
                                <th>Nama Parameter</th>
                                <th>aksi</th>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach($ksb_parameters as $i=>$ksb_parameter )
                                <tr>
                                  <td>{{ $i+1 }}</td>
                                  <td>
                                    {{ $ksb_parameter->parameter }}
                                  </td>
                                  <td><a href="deleteParameter/{{ $ksb_parameter->id }}" class="btn-sm btn-danger">hapus</a></td>
                                </tr>
                              @endforeach
                            </tbody>
                          </table>
                    </div>
                </div>
                <br><br>
                <div class="card">
                    <div class="card-header">{{ __('Tambah Parameter') }}</div>
                    <div class="card-body">
                        <form method="POST" action="{{route('createParameter')}}">
                            @csrf
    
                            <div class="mb-3 row">
                                <label for="id_layanan" class="col-md-4 col-form-label text-md-right">{{ __('ID Layanan') }}</label>
                                <div class="col-md-6">
                                    {{-- <input name="id_layanan" type="text" class="form-control" id="id_layanan" value="" required autocomplete="id_layanan"> --}}
                                    <input name="id_layanan" type="text" class="form-control" id="id_layanan" value="{{$ksb_layanan->id}}" required autocomplete="id_layanan">
                                </div>
                            </div>
    
                            <div class="mb-3 row">
                                <label for="nama_layanan" class="col-md-4 col-form-label text-md-right">{{ __('Nama Layanan') }}</label>
                                <div class="col-md-6">
                                  {{-- <input name="nama_layanan" type="text" class="form-control" id="nama_layanan" value="" required autocomplete="nama_layanan"> --}}
                                  <input name="nama_layanan" type="text" class="form-control" id="nama_layanan" value="{{$ksb_layanan->nama_layanan}}" required autocomplete="nama_layanan">
                                </div>
                            </div>
    
                            <div class="mb-3 row">
                                <label for="parameter" class="col-md-4 col-form-label text-md-right">{{ __('Parameter') }}</label>
                                <div class="col-md-6">
                                  <input name="parameter" type="text" class="form-control" id="parameter" value="{{old('parameter')}}" required autocomplete="user_name">
                                </div>
                            </div>
    
                          
    
                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn-sm btn-primary">Buat Parameter</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection