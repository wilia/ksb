@extends('layouts.header')

@section('content')
<main class="hoc container clear"> 
<div class="scrollable">
  <a href="tambahmetode" class="btn btn-primary active" role="button" aria-pressed="true" style="float: right;">Tambah Data</a>
  <br>
  <br>
    <table>
      <thead>
        <tr>
          <th>NO</th>
          <th>Nama Instansi</th>
          <th>Nama Layanan</th>
          <th>Nama Layanan</th>
          <th>Jenis Metode</th>
          <th>Status</th>
          <th>Aksi</th>
        </tr>
      </thead>
      <tbody>
        @forelse ($ksb_metode_layanans as $ksb_metode_layanan)
        <tr>
            <td>{{$loop->iteration}}</td>
            <td>{{$ksb_metode_layanan->nama_instansi}}</td>
            <td>{{$ksb_metode_layanan->nama_layanan}}</td>
            <td>{{$ksb_metode_layanan->nama_metode}}</td>
            <td>{{$ksb_metode_layanan->jenis_metode}}</td>
            <td>{{$ksb_metode_layanan->status}}</td>
          <td><a href="editmetode/{{ $ksb_metode_layanan->id }}" class="btn btn-info"><i class=" fas fa-edit"></i></a>||<a href="hapusMetode/{{ $ksb_metode_layanan->id }}" class="btn btn-danger"><i class=" fas fa-trash-alt"></i></a></td>
        </tr>
        @empty
        <td colspan="4" class="text-center"></td>
        @endforelse
      </tbody>
    </table>
    {{-- <div class="card-footer">
      {{$ksb_metode_layanans->links()}}
    </div> --}}
  </div>
</main>
@endsection