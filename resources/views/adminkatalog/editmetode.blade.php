@extends('layouts.header')

@section('content')
<main class="hoc container clear"> 
  <h1>Form Edit Metode</h1>
  <form action="{{url("updateMetode",$ksb_metode_layanans->id)}}" method="POST">
    @csrf
    <div class="form-row">
      <div class="form-group col-md-6">
        <label for="nama_instansi">Nama Instansi</label>
        <input name="nama_instansi" type="text" class="form-control" id="nama_instansi" value="{{$ksb_metode_layanans->nama_instansi}}">
      </div>
    </div>
    <div class="form-row">
      <div class="form-group col-md-6">
        <label for="nama_layanan">Nama Layanan</label>
        <input name="nama_layanan" type="text" class="form-control" id="nama_layanan" value="{{$ksb_metode_layanans->nama_layanan}}">
      </div>
    </div>
    <div class="form-row">
      <div class="form-group col-md-6">
        <label for="nama_metode">Nama Metode</label>
        <input name="nama_metode" type="text" class="form-control" id="nama_metode" value="{{$ksb_metode_layanans->nama_metode}}">
      </div>
    </div>
    <div class="form-row">
      <div class="form-group col-md-6">
        <label for="jenis_metode">Jenis Metode</label>
        <input name="jenis_metode" type="text" class="form-control" id="jenis_metode" value="{{$ksb_metode_layanans->jenis_metode}}">
      </div>
    </div>
    <div class="form-row">
      <div class="form-group col-md-6">
        <label for="status">Status</label>
        <select name="status" class="form-control" id="status" value="{{$ksb_metode_layanans->status}}">
          <option selected>Pilih ...</option>
          <option value="aktif">Akif</option>
          <option value="tidak aktif">Tidak Aktif</option>
        </select>
      </div>
    </div>
    <button type="reset" class="btn btn-danger">Batal</button>
    <button type="submit" class="btn btn-primary">OK</button>
  </form>
</main>
@endsection