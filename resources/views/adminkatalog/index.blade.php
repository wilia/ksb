@extends('layouts.header')

@section('content')
<div class="wrapper row3">
  <main class="hoc container clear"> 
    <!-- main body -->
    <!-- ################################################################################################ -->
    <div class="sectiontitle">
      <h6 class="heading">Menu Layanan Katalog Service Bus</h6>
    </div>
    <div class="group center btmspace-80">
      <article class="one_third first"><a class="ringcon btmspace-50" href="{{route('adminkatalogs.layanantersedia')}}"><i class="fas fa-list"></i></a>
        <h6 class="heading">Daftar Layanan Tersedia</h6>
      </article>
      <article class="one_third"><a class="ringcon btmspace-50" href="{{route('adminkatalogs.layanandiajukan')}}"><i class="fas fa-tasks"></i></a>
        <h6 class="heading">Daftar Layanan yang Diajukan</h6>
      </article>
      <article class="one_third"><a class="ringcon btmspace-50" href="metodelayanan"><i class="fas fa-list"></i></a>
        <h6 class="heading">Metode Layanan</h6>
      </article>
    </div>
    <!-- ################################################################################################ -->
    <!-- / main body -->
    <div class="clear"></div>
  </main>
</div>

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<a id="backtotop" href="#top"><i class="fas fa-chevron-up"></i></a>
<!-- JAVASCRIPTS -->
<script src="scripts/jquery.min.js"></script>
<script src="scripts/jquery.backtotop.js"></script>
<script src="scripts/jquery.mobilemenu.js"></script>
</body>
</html>
@endsection