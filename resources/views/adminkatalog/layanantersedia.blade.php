@extends('layouts.header')

@section('content')
<main class="hoc container clear"> 
<div class="scrollable">
  <h1>Daftar Layanan Tersedia</h1>
    <table>
      <thead>
        <tr>
          <th>No</th>
          <th>Nama Pemesan</th>
          <th>Instansi</th>
          <th>Nama Layanan</th>
          {{-- <th>Tanggal Mulai</th>
          <th>Tanggal Berakhir</th> --}}
          <th>Status</th>
          <th>Pratinjau</th>
          <th>Aksi</th>
        </tr>
      </thead>
      <tbody>
        @forelse ($ksb_layanans as $ksb_layanan)
        <tr>
          <td>{{$loop->iteration}}</td>
          <td>{{$ksb_layanan->nama_pemesan}}</td>
          <td>{{$ksb_layanan->instansi}}</td>
          <td>{{$ksb_layanan->nama_layanan}}</td>
          {{-- <td>{{$ksb_layanan->batas_mulai}}</td>
          <td>{{$ksb_layanan->batas_selesai}}</td> --}}
          <td>{{$ksb_layanan->status}}</td>
          <td><a href="pratinjau/{{ $ksb_layanan->id }}" class="btn-sm btn-primary"><i class=" fas fa-search-plus"></i></a></td>
          <td><a href="formedit/{{ $ksb_layanan->id }}" class="btn-sm btn-info">Edit</a>||<a href="hapusLayanan/{{ $ksb_layanan->id }}" class="btn-sm btn-danger">hapus</a>||<a href="formparameter/{{ $ksb_layanan->id }}" class="btn-sm btn-info">parameter</a></td>
        </tr>
        @empty
        <td colspan="4" class="text-center"></td>
        @endforelse
      </tbody>
    </table>
  </div>
</main>
@endsection