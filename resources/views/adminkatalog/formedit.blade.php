@extends('layouts.header')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="card">
        <div class="card-header">{{ __('Edit Layanan') }}</div>
        <div class="card-body">
            <form method="POST" action="{{url("ubahLayanan",$ksb_layanans->id)}}">
                @csrf

                <div class="mb-3 row">
                    <label for="nama_pemesan" class="col-md-4 col-form-label text-md-right">Nama Requester</label>
                    <div class="form-group col-md-6">
                    <input name="nama_pemesan" type="text" class="form-control" id="nama_pemesan" value="{{$ksb_layanans->nama_pemesan}}">
                  </div>
                </div>
                <div class="mb-3 row">
                    <label for="instansi" class="col-md-4 col-form-label text-md-right">Instansi</label>
                    <div class="form-group col-md-6">
                    <input name="instansi" type="text" class="form-control" id="instansi" value="{{$ksb_layanans->instansi}}">
                  </div>
                </div>
                <div class="mb-3 row">
                    <label for="nama_layanan" class="col-md-4 col-form-label text-md-right">Nama Layanan</label>
                    <div class="form-group col-md-6">
                    <select name="nama_layanan" class="form-control">
                      <option>Pilih Metode</option>
                      @foreach($metod_layan as $i=>$metod_layanan )
                          <option value="{{ $metod_layanan->nama_metode }}">{{ $metod_layanan->nama_metode }}</option>
                      @endforeach
                  </select>
                  </div>
                </div>
                <div class="mb-3 row">
                    <label for="status" class="col-md-4 col-form-label text-md-right">Status</label>
                    <div class="form-group col-md-6">
                    <select name="status" class="form-control" id="status" value="{{$ksb_layanans->status}}">
                      <option selected>Pilih ...</option>
                      <option value="aktif">Akif</option>
                      <option value="tidak aktif">Tidak Aktif</option>
                    </select>
                  </div>
                </div>
                <div class="form-group row mb-0">
                  <div class="col-md-6 offset-md-4">
                    <button type="reset" class="btn btn-danger">Batal</button>
                    <button type="submit" class="btn btn-primary">Update</button>
                  </div>
              </div>
            </form>
        </div>
    </div>
    </div>
  </div>
</div>
{{-- <main class="hoc container clear"> 
  <h1>Form Edit Layanan</h1>
  <form>
    <div class="form-row">
      <div class="form-group col-md-6">
        <label for="inputEmail4">Nama Requester</label>
        <input type="email" class="form-control" id="inputEmail4">
      </div>
      <div class="form-group col-md-6">
        <label for="inputPassword4">Instansi</label>
        <input type="password" class="form-control" id="inputPassword4">
      </div>
    </div>
    <div class="form-group">
      <label for="inputAddress">Nama Layanan</label>
      <select id="inputState" class="form-control">
        <option selected>Pilih nama layanan...</option>
        <option>...</option>
      </select>
    </div>
    <div class="form-group">
      <label for="inputAddress2">URL Layanan</label>
      <input type="text" class="form-control" id="inputAddress2" placeholder="">
    </div>
    <div class="form-row">
      <div class="form-group col-md-6">
        <label for="inputCity">Parameter</label>
        <input type="text" class="form-control" id="inputCity">
      </div>
      <div class="form-group col-md-6">
        <label for="inputState">Status</label>
        <select class="form-control" id="inlineFormCustomSelect">
          <option selected>Pilih ...</option>
          <option value="aktif">Akif</option>
          <option value="tidak aktif">Tidak Aktif</option>
        </select>
      </div>
    </div>
    <div class="form-group">
      <label for="inputState">Query SQL</label>
      <textarea class="form-control" id="queryql" rows="3"></textarea>
    </div>
    <div class="form-row">
      <div class="form-group col-md-6">
        <label for="inputEmail4">Tanggal Awal Aktif</label>
        <input type="email" class="form-control" id="inputEmail4">
      </div>
      <div class="form-group col-md-6">
        <label for="inputPassword4">Tanggal Terakhir Akrif</label>
        <input type="password" class="form-control" id="inputPassword4">
      </div>
    </div>
    <button type="reset" class="btn btn-danger">Batal</button>
    <button type="submit" class="btn btn-primary">Edit</button>
  </form>
</main> --}}
@endsection