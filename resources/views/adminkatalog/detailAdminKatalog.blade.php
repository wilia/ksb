@extends('layouts.header')

@section('content')

<main class="hoc container clear"> 
    <div class="scrollable">
        <div class="row justify-content-center">
            <div class="col-md-8">
            <div class="card">
                 <div class="card-header">
                    Detail Data Admin Katalog
                </div>
                <div class="card-body">
                    <table>
                        <tr>
                            <td>ID</td>
                            <td>{{$user->id}}</td>
                        </tr>
                        <tr>
                            <td>Nama Lengkap</td>
                            <td>{{$user->nama_lengkap}}</td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>{{$user->user_email}}</td>
                        </tr>
                        <tr>
                            <td>Username</td>
                            <td>{{$user->user_name}}</td>
                        </tr>
                    </table>
                </div>
            </div>
            </div>
        </div>
    </div>


</main>
@endsection