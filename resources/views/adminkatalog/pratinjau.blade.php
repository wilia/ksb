@extends('layouts.header')

@section('content')
<main class="hoc container clear"> 
  <h1>Pratinjau</h1>
  <table>
    <thead>
      <tr>
        <th>No</th>
        <th>Nama Parameter</th>
        <th>Nilai</th>
      </tr>
    </thead>
    <tbody>
      @foreach($ksb_parameters as $i=>$ksb_parameter )
        <tr>
          <td>{{ $i+1 }}</td>
          <td>
            {{ $ksb_parameter->parameter }}
          <input type="hidden" class="urlParameter-{{ $i+1 }}" value="{{ $ksb_parameter->parameter }}">
          </td>
          <td><input type="nilai" id="nilai" value="" class="{{ $ksb_parameter->parameter }}" onchange="nilaiParameter('{{ $ksb_parameter->parameter }}')"></td>
        </tr>
      @endforeach
    </tbody>
  </table>
  <form>
    <select name="type_print" class="type_print">
      <option value="json">Json</option>
      <option value="xml">XML</option>
    </select>
    <div class="form-group">
      <label for="inputAddress2">URL Layanan</label>
      <input type="text" class="form-control urlLayanan" id="inputAddress2" value="" disabled>
    </div>
      <a class="btn btn-primary tampilJson" name="print" value = "json">Tampil Json</a>
  </form>
  <input type="hidden" class="countParameter" value="{{ $ksb_parameters->count() }}">
</main>
@endsection

@section('js')
<script type="text/javascript">
  // baseurl aplikasi
  var APP_URL = {!! json_encode(url('/tampilJson?')) !!}
  // jumlah semua paramete
  var countParameter = parseInt($('.countParameter').val())+1;
  // baseurl+parameter
  var url = APP_URL+"";
  // variabel tampung parameter
  var allParameters = [];
  // push data parameter ke allParameters
  for (var i = 1; i < countParameter; ++i) {
    allParameters.push({parameter: $('.urlParameter-'+i).val(), nilai: ''});
  }
  // generate url diawal
  $.each(allParameters, function(i, item) {
      url += item.parameter+'='+item.nilai+"&";
  });

  function nilaiParameter(parameter) {
    var nilai = $('.'+parameter).val();
    // fungsi delete json
    var newData = removeJsonByParameter(parameter);
    allParameters = newData;
    // fungsi add json
    allParameters.push({parameter: parameter, nilai: nilai});

    // generate url
    url = APP_URL+"";
    $.each(allParameters, function(i, item) {
        url += item.parameter+'='+item.nilai+"&";
    });
    url = url.slice(0, -1);

    // replace class urlLayanan
    $('.urlLayanan').val(url);
  }

  function removeJsonByParameter(parameter) {
    return allParameters.filter(function(data) {
      if (data.parameter == parameter) {
        return false;
      }
      return true;
    });
  }

  $('.urlLayanan').val(url);

  $(".tampilJson").click(function(){
    var typePrint = $('.type_print').val();
    window.location.href = url+'&type_print='+typePrint;
  });
  
</script>
@endsection