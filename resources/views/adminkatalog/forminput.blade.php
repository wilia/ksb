@extends('layouts.header')

@section('content')
<body id="top" style="background-color: #f4f4f4;">
  <div class="container" style="background-color: #f4f4f4;">
    <div class="row justify-content-center">
        <div class="col-md-8">
  <h1>Form Tambah Layanan</h1>
  <form action="{{route('buatLayanan')}}" method="POST">
    @csrf
    <div class="form-row">
      <div class="form-group col-md-6">
        <label for="nama_pemesan">Nama Requester</label>
        <input name="nama_pemesan" type="text" class="form-control" id="nama_pemesan">
      </div>
      <div class="form-group col-md-6">
        <label for="instansi">Instansi</label>
        <input name="instansi" type="text" class="form-control" id="instansi">
      </div>
    </div>
    <div class="form-group">
      <label for="nama_layanan">Nama Layanan</label>
      {{-- <select name="nama_layanan" id="nama_layanan" class="form-control">
        <option selected>Pilih nama layanan...</option>
        <option>...</option>
      </select> --}}
      <select name="nama_layanan" class="form-control">
        <option>Pilih Metode</option>
        @foreach($metod_layan as $i=>$metod_layanan )
            <option value="{{ $metod_layanan->nama_metode }}">{{ $metod_layanan->nama_metode }}</option>
        @endforeach
    </select>
    </div>
    <div class="form-group">
      <label for="url_layanan">URL Layanan</label>
      <input name="url_layanan" type="text" class="form-control" id="url_layanan" placeholder="">
    </div>
    <div class="form-row">
      <div class="form-group col-md-6">
        <label for="status">Status</label>
        <select name="status" class="form-control" id="status">
          <option selected>Pilih ...</option>
          <option value="aktif">Akif</option>
          <option value="tidak aktif">Tidak Aktif</option>
        </select>
      </div>
    </div>
    <div class="form-group">
      <label for="query_layanan">Query SQL</label>
      <textarea name="query_layanan" class="form-control" id="query_layanan" rows="3"></textarea>
    </div>
    <div class="form-row">
      <div class="form-group col-md-6">
        <label for="batas_mulai">Batas Mulai</label>
        <input name="batas_mulai" type="date" class="form-control" id="batas_mulai">
      </div>
      <div class="form-group col-md-6">
        <label for="batas_selesai">Batas Selesai</label>
        <input name="batas_selesai" type="date" class="form-control" id="batas_selesai">
      </div>
    </div>
    <button type="reset" class="btn btn-danger">Batal</button>
    <button type="submit" class="btn btn-primary">Buat</button>
  </form>
  </div>
  </div>
  </div>
</body>
@endsection