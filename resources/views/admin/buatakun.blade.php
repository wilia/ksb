@extends('layouts.headeradmin')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Form Buat Akun') }}</div>
                    <div class="card-body">
                        <form method="POST" action="{{url("createAkun",$ksb_pengajuan_akuns->id)}}">
                            @csrf
    
                            <div class="mb-3 row">
                                <label for="user_role_grup_id" class="col-md-4 col-form-label text-md-right">{{ __('Nama Grup') }}</label>
                                <div class="col-md-6">
                                    <select class="form-control" name="user_role_grup_id">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                      </select>
                                </div>
                            </div>
    
                            <div class="mb-3 row">
                                <label for="nama_lengkap" class="col-md-4 col-form-label text-md-right">{{ __('Nama Lengkap') }}</label>
                                <div class="col-md-6">
                                  <input name="nama_lengkap" type="text" class="form-control" id="nama_lengkap" value="{{$ksb_pengajuan_akuns->nama_lengkap}}" required autocomplete="nama_lengkap">
                                </div>
                            </div>
    
                            <div class="mb-3 row">
                                <label for="user_name" class="col-md-4 col-form-label text-md-right">{{ __('Username') }}</label>
                                <div class="col-md-6">
                                  <input name="user_name" type="text" class="form-control" id="user_name" value="{{$ksb_pengajuan_akuns->user_name}}" required autocomplete="user_name">
                                </div>
                            </div>
    
                            <div class="mb-3 row">
                                <label for="user_email" class="col-md-4 col-form-label text-md-right">{{ __('Email') }}</label>
                                <div class="col-md-6">
                                  <input name="user_email" type="email" class="form-control" id="user_email" value="{{$ksb_pengajuan_akuns->user_email}}" required autocomplete="user_email">
                                </div>
                            </div>
    
                            <div class="mb-3 row">
                                <label for="user_password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
                                <div class="col-md-6">
                                  <input name="user_password" type="password" class="form-control" id="user_password" value="{{$ksb_pengajuan_akuns->user_password}}" required autocomplete="user_password">
                                </div>
                            </div>
    
                            <div class="mb-3 row">
                                <label for="nip" class="col-md-4 col-form-label text-md-right">{{ __('NIP') }}</label>
                                <div class="col-md-6">
                                  <input name="nip" type="text" class="form-control" id="nip" value="{{$ksb_pengajuan_akuns->nip}}" required autocomplete="nip">
                                </div>
                            </div>
    
                            <div class="mb-3 row">
                                <label for="status" class="col-md-4 col-form-label text-md-right">{{ __('Status') }}</label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="status" id="status" value="aktif">
                                    <label class="form-check-label" for="status">Aktif</label>
                                  </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="status" id="status" value="tidak_aktif">
                                    <label class="form-check-label" for="status">Tidak Aktif</label>
                                </div>
                            </div>
    
                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">Buat Akun</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection