@extends('layouts.headeradmin')

@section('content')

<main class="hoc container clear"> 
<div class="scrollable">
    <h2>daftar akun user</h2>
  
    @if (Session::has('sukses'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      <button aria-label="Close" class="close" data-dismiss="alert" type="button">
        <span aria-hidden="true"> ×</span>
      </button>
      <strong>
      {{ Session::get('sukses') }}
      </strong>
    </div>
  @endif
  <div class="d-grid gap-2 d-md-flex justify-content-md-end">
    <form class="form-inline my-2 my-lg-0" method="GET" action="akunrequester">
      <input name="cari" class="form-control mr-sm-2" type="search" placeholder="nama user" aria-label="nama user">
      <button class="btn btn-primary my-2 my-sm-0" type="submit">Cari</button>

    </form>
  </div>
  <br/>

    <table>
      <thead>
        <tr>
            <th>NO</th>
            <th>ID</th>
            {{-- <th>Active</th> --}}
            <th>Nama Lengkap</th>
            <th>Email</th>
            <th>User Grup ID</th>
            <th>Aksi</th>
            <th>Status</th>
        </tr>
      </thead>
      <tbody>
        @forelse ($users as $user)
        <tr>
            <td>{{$loop->iteration}}</td>
            <td>{{$user->id}}</td>
            {{-- <td>{{$user->user_password}}</td> --}}
            <td>{{$user->nama_lengkap}}</td>
            <td>{{$user->user_email}}</td>
            <td>{{$user->user_role_grup_id}}</td>


            @if ($user->deleted_date == null)
            <td><a href="/admins/delete/{{ $user->id }}" type="button" class="btn btn-danger">Nonaktifkan</a></td>
            <td>Aktif</td>
            @else
            <td><a href="/admins/restore/{{ $user->id }}" type="button" class="btn btn-primary">Aktifkan</a></td>
            <td>Tidak Aktif</td>
            @endif
            
        </tr>
            
        @empty
            <td colspan="4" class="text-center">Data Barang Tidak Ada...</td>
        @endforelse
      </tbody>
    </table>
  </div>
</main>
@endsection