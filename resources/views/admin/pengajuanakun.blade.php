@extends('layouts.headeradmin')

@section('content')
<main class="hoc container clear"> 
<div class="scrollable">
    <h2>Akun yang Diajukan Requester</h2>
    <br>
    <table>
      <thead>
        <tr>
            {{-- <th>NIK</th> --}}
            <th>Role Grup ID</th>
            <th>Nama Lengkap</th>
            <th>Username</th>
            <th>Instansi</th>
            <th>Email</th>
            <th>Surat Tugas</th>
            <th>KTP</th>
            <th>Keterangan</th>
            <th>Aksi</th>
        </tr>
      </thead>
      <tbody>
        @forelse ($ksb_pengajuan_akuns as $ksb_pengajuan_akun)
        <tr>
          {{-- <td>{{$ksb_pengajuan_akun->nip}}</td> --}}
          <td>{{$ksb_pengajuan_akun->user_role_grup_id}}</td>
          <td>{{$ksb_pengajuan_akun->nama_lengkap}}</td>
          <td>{{$ksb_pengajuan_akun->user_name}}</td>
          <td>{{$ksb_pengajuan_akun->instansi}}</td>
          <td>{{$ksb_pengajuan_akun->user_email}}</td>
          <td>{{$ksb_pengajuan_akun->suratTugas}}</td>
          <td>{{$ksb_pengajuan_akun->ktp}}</td>
          @if ($ksb_pengajuan_akun->active == 2)
          <td>Diterima</td>
          @elseif ($ksb_pengajuan_akun->active == 1)
          <td>Ditolak</td>
          @else
          <td>Belum diproses</td>
          @endif
          <td>
            <a href="/tolakAkun/{{ $ksb_pengajuan_akun->id }}" type="button" class="btn btn-danger"><i class="fa fa-times-circle"></i></a>||<a href="buatakun/{{ $ksb_pengajuan_akun->id }}" type="button" class="btn btn-primary"><i class=" fas fa-edit"></i></a>
            
          </td>
            
        </tr>
            
        @empty
            <td colspan="4" class="text-center"></td>
        @endforelse
      </tbody>
    </table>
  </div>
</main>
@endsection