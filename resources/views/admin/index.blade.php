@extends('layouts.headeradmin')

@section('content')
  <div class="wrapper row3">
    <main class="hoc container clear"> 
      <!-- main body -->
      <div class="sectiontitle">
        <h6 class="heading">Menu Layanan Katalog Service Bus</h6>
      </div>
      <div class="group center btmspace-80">
        <article class="one_third first"><a class="ringcon btmspace-50" href=""><i class=" fas fa-users"></i></a>
          <h6 class="heading">Daftar Akun Requester</h6>
        </article>
        <article class="one_third"><a class="ringcon btmspace-50" href="#"><i class="fas fa-user-plus"></i></a>
          <h6 class="heading">Daftar Pengajuan Akun Requester</h6>
        </article>
        <article class="one_third"><a class="ringcon btmspace-50" href="#"><i class="fas fa-edit"></i></a>
          <h6 class="heading">Buat Akun Admin dan Admin Katalog</h6>
        </article>
      </div>
      <div class="clear"></div>
    </main>
  </div>
    <div class="wrapper row5">
    <div id="copyright" class="hoc clear"> 
      <p class="fl_left">Copyright &copy; 2018 - All Rights Reserved - <a href="#">Domain Name</a></p>
      <p class="fl_right">Template by <a target="_blank" href="https://www.os-templates.com/" title="Free Website Templates">OS Templates</a></p>
    </div>
  </div>
  <a id="backtotop" href="#top"><i class="fas fa-chevron-up"></i></a>
  <!-- JAVASCRIPTS -->
  <script src="scripts/jquery.min.js"></script>
  <script src="scripts/jquery.backtotop.js"></script>
  <script src="scripts/jquery.mobilemenu.js"></script>
  </body>
  </html>
@endsection
