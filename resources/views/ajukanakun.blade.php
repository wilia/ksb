<!DOCTYPE html>
<!--
Template Name: Wavefire
Author: <a href="https://www.os-templates.com/">OS Templates</a>
Author URI: https://www.os-templates.com/
Copyright: OS-Templates.com
Licence: Free to use under our free template licence terms
Licence URI: https://www.os-templates.com/template-terms
-->
<html lang="">
<!-- To declare your language - read more here: https://www.w3.org/International/questions/qa-html-language-declarations -->
<head>
<title>KatalogServiceBus</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="{{asset('styles/layout.css')}}" rel="stylesheet" type="text/css" media="all">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
<style>
    .card-header{
        font-size: 14pt;
        color: white;
    }
</style>
</head>
<body id="top" style="background-color: #f4f4f4;">
    <div class="container" style="background-color: #f4f4f4;">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header" style="background-color: #95103b;">{{ __('Form Pengajuan Akun') }}</div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('create') }}">
                            @csrf
                            
                            <div class="mb-3 row">
                                <label for="user_role_grup_id" class="col-md-4 col-form-label text-md-right">{{ __('Role Grup') }}</label>
                                <div class="col-md-6">
                                    <select class="form-control" name="user_role_grup_id">
                                        <option value="2">eCatalogue & ePurchasing - Penyedia</option>
                                        <option value="3">eCatalogue & ePurchasing - Panitia</option>
                                        <option value="4">eCatalogue & ePurchasing - PPK</option>
                                        <option value="6">eCatalogue & ePurchasing - Admin</option>
                                        <option value="10">eCatalogue & ePurchasing - Distributor</option>
                                        <option value="11">eCatalogue & ePurchasing - Pejabat Pengadaan</option>
                                        <option value="12">eCatalogue & ePurchasing - Auditor</option>
                                        <option value="20">eCatalogue & ePurchasing - Admin Struktural</option>
                                        <option value="17">eCatalogue & ePurchasing - Helpdesk</option>
                                        <option value="8">CMS - Admin</option>
                                        <option value="9">CMS - Mananger</option>
                                        <option value="13">CMS - Monev</option>
                                        <option value="15">eCatalogue - Sekretariat</option>
                                      </select>
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label for="nip" class="col-md-4 col-form-label text-md-right">{{ __('NIP') }}</label>
                                <div class="col-md-6">
                                  <input name="nip" type="text" class="form-control" id="nip" value="{{old('nip')}}" required autocomplete="nip">
                                </div>
                            </div>
    
                            <div class="mb-3 row">
                                <label for="nama_lengkap" class="col-md-4 col-form-label text-md-right">{{ __('Nama Lengkap') }}</label>
                                <div class="col-md-6">
                                  <input name="nama_lengkap" type="text" class="form-control" id="nama_lengkap" value="{{old('nama_lengkap')}}" required autocomplete="nama_lengkap">
                                </div>
                            </div>
    
                            <div class="mb-3 row">
                                <label for="user_name" class="col-md-4 col-form-label text-md-right">{{ __('Username') }}</label>
                                <div class="col-md-6">
                                  <input name="user_name" type="text" class="form-control" id="user_name" value="{{old('user_name')}}" required autocomplete="user_name">
                                </div>
                            </div>
    
                            <div class="mb-3 row">
                                <label for="user_email" class="col-md-4 col-form-label text-md-right">{{ __('Email') }}</label>
                                <div class="col-md-6">
                                  <input name="user_email" type="email" class="form-control" id="user_email" value="{{old('user_email')}}" required autocomplete="user_email">
                                </div>
                            </div>
    
                            <div class="mb-3 row">
                                <label for="user_password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
                                <div class="col-md-6">
                                  <input name="user_password" type="password" class="form-control" id="user_password" value="{{old('user_password')}}" required autocomplete="user_password">
                                </div>
                            </div>
    
                            <div class="mb-3 row">
                                <label for="instansi" class="col-md-4 col-form-label text-md-right">{{ __('Instansi') }}</label>
                                <div class="col-md-6">
                                  <input name="instansi" type="text" class="form-control" id="instansi" value="{{old('instansi')}}" required autocomplete="instansi">
                                </div>
                            </div>
    
                            <div class="form-group row">
                                <label for="suratTugas" class="col-md-4 col-form-label text-md-right">{{ __('Surat Tugas') }}</label>
    
                                <div class="col-md-6">
                                    <input id="suratTugas" type="file" class="form-control" name="suratTugas" required autocomplete="suratTugas">
                                </div>
                            </div>
    
                            <div class="form-group row">
                                <label for="ktp" class="col-md-4 col-form-label text-md-right">{{ __('KTP') }}</label>
    
                                <div class="col-md-6">
                                    <input id="ktp" type="file" class="form-control" name="ktp" required autocomplete="ktp">
                                </div>
                            </div>
    
                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <a href="/" class="btn btn-danger">Batal</a>
                                    <button type="submit" class="btn btn-success">Ajukan Akun</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<main class="py-4">
    @yield('content')
</main>

<!-- JAVASCRIPTS -->
<script src="scripts/jquery.min.js"></script>
<script src="scripts/jquery.backtotop.js"></script>
<script src="scripts/jquery.mobilemenu.js"></script>
</body>
</html>