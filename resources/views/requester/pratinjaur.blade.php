@extends('layouts.headerequester')

@section('content')
<main class="hoc container clear"> 
  <h1>Pratinjau</h1>
  <table>
    <thead>
      <tr>
        <th>No</th>
        <th>Nama Parameter</th>
        <th>Tipe Parameter</th>
        <th>Nilai</th>
        <th>Filter</th>
        
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>1</td>
        <td>Kode</td>
        <td>Varchar</td>
        <td><input type="nilai" id="nilai"></td>
        <td>Ya</td>
      </tr>
    </tbody>
  </table>
  <form>
    <div class="form-group">
        <label for="inputAddress2">URL Layanan</label>
        <input type="text" class="form-control" id="inputAddress2" placeholder="">
      </div>
      <button type="submit" class="btn btn-primary">Tampilkan</button>
  </form>
</main>
@endsection