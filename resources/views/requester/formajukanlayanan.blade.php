@extends('layouts.headerequester')

@section('content')
<main class="hoc container clear">
  <div class="row justify-content-center">
  <div class="col-md-8">
  <div class="card">
   <div class="card-header">{{ __('Pengajuan Layanan') }}</div>
   <div class="card-body">
  <form action="{{route('createLayanan')}}" method="POST">
    @csrf
    <div class="mb-3 row">
        <label for="nama_pemesan" class="col-md-4 col-form-label text-md-right">Nama Pemesan</label>
        <div class="form-group col-md-6">
        <input name="nama_pemesan" type="text" class="form-control" id="nama_pemesan" value="{{ Auth::user()->nama_lengkap }}">
      </div>
    </div>
    <div class="mb-3 row">
        <label for="instansi" class="col-md-4 col-form-label text-md-right">Instansi</label>
        <div class="form-group col-md-6">
        <input name="instansi" type="text" class="form-control" id="instansi" value="{{old('instansi')}}">
      </div>
    </div>
    <div class="mb-3 row">
      <label for="nama_layanan" class="col-md-4 col-form-label text-md-right">Nama Layanan</label>
      <div class="form-group col-md-6">
        <select name="nama_layanan" class="form-control">
          <option>Pilih Metode</option>
          @foreach($metod_layan as $i=>$metod_layanan )
              <option value="{{ $metod_layanan->nama_metode }}">{{ $metod_layanan->nama_metode }}</option>
          @endforeach
      </select>
    </div>
    </div>
    {{-- <div class="form-row">
    <div class="form-group col-md-6">
      <label for="alamat_ip">Alamat IP</label>
      <input name="alamat_ip" type="text" class="form-control" id="alamat_ip" value="{{old('alamat_ip')}}">
    </div>
    </div> --}}
    {{-- <div class="form-row">
        <div class="form-group col-md-6">
          <label for="acces_key">Accyes Key</label>
          <input name="acces_key" type="text" class="form-control" id="acces_key" value="{{old('acces_key')}}">
        </div>
    </div> --}}
    <div class="mb-3 row">
          <label for="keterangan" class="col-md-4 col-form-label text-md-right">Keterangan</label>
          <div class="form-group col-md-6">
          <input name="keterangan" type="text" class="form-control" id="keterangan" value="{{old('keterangan')}}">
        </div>
    </div>
    <div class="form-group row mb-0">
      <div class="col-md-6 offset-md-4">
        <button type="reset" class="btn btn-danger">Batal</button>
        <button type="submit" class="btn btn-primary">OK</button>
      </div>
      </div>
  </form>
   </div>
  </div>
  </div>
  </div>
</main>
@endsection