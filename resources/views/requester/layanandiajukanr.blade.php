@extends('layouts.headerequester')

@section('content')
<main class="hoc container clear"> 
<div class="scrollable">
  <h1>Layanan yang Diajukan</h1>
    <table>
      <thead>
        <tr>
          <th>Nama Pemesan</th>
          <th>Nama Layanan</th>
          <th>Keterangan</th>
          <th>Status Pengajuan</th>
          <th>Aksi</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          @forelse ($ksb_pengajuan_layanans as $ksb_pengajuan_layanan)
          <tr>
            <td>{{$ksb_pengajuan_layanan->nama_pemesan}}</td>
            <td>{{$ksb_pengajuan_layanan->nama_layanan}}</td>
            <td>{{$ksb_pengajuan_layanan->keterangan}}</td>
            @if ($ksb_pengajuan_layanan->status_pengajuan == 1)
           <td>Diterima</td>
            @elseif ($ksb_pengajuan_layanan->status_pengajuan == 2)
            <td>Ditolak</td>
            @else
            <td>Belum diproses</td>
            @endif
            {{-- <td>{{$ksb_pengajuan_layanan->status_pengajuan}}</td> --}}
            <td>
              <a href="" type="button" class="btn btn-danger"><i class="fa fa-times-circle"></i></a>||<a href="formeditpengajuan/{{ $ksb_pengajuan_layanan->id }}" type="button" class="btn btn-primary"><i class=" fas fa-edit"></i></a>
              
            </td>
              
          </tr>
              
          @empty
              <td colspan="4" class="text-center"></td>
          @endforelse
      </tbody>
    </table>
  </div>
</main>
@endsection