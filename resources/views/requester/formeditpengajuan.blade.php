@extends('layouts.headerequester')

@section('content')
<main class="hoc container clear"> 
  <div class="row justify-content-center">
    <div class="col-md-8">
    <div class="card">
     <div class="card-header">{{ __('Edit Pengajuan Layanan') }}</div>
     <div class="card-body">
  <form action="{{url("updateLayanan",$ksb_pengajuan_layanans->id)}}" method="POST">
    @csrf
    <div class="mb-3 row">
        <label for="nama_pemesan" class="col-md-4 col-form-label text-md-right">Nama Pemesan</label>
        <div class="form-group col-md-6">
        <input name="nama_pemesan" type="text" class="form-control" id="nama_pemesan" value="{{$ksb_pengajuan_layanans->nama_pemesan}}">
        </div>
    </div>
    <div class="mb-3 row">
        <label for="instansi" class="col-md-4 col-form-label text-md-right">Instansi</label>
      <div class="form-group col-md-6">
        <input name="instansi" type="text" class="form-control" id="instansi" value="{{$ksb_pengajuan_layanans->instansi}}">
      </div>
    </div>
    <div class="mb-3 row">
      <label for="nama_layanan" class="col-md-4 col-form-label text-md-right">Nama Layanan</label>
    <div class="form-group col-md-6">
      <input name="nama_layanan" type="text" class="form-control" id="nama_layanan" value="{{$ksb_pengajuan_layanans->nama_layanan}}">
    </div>
    </div>
    <div class="mb-3 row">
          <label for="keterangan" class="col-md-4 col-form-label text-md-right">Keterangan</label>
        <div class="form-group col-md-6">
          <input name="keterangan" type="text" class="form-control" id="keterangan" value="{{$ksb_pengajuan_layanans->keterangan}}">
        </div>
    </div>
    <div class="form-group row mb-0">
      <div class="col-md-6 offset-md-4">
    <button type="reset" class="btn btn-danger">Batal</button>
    <button type="submit" class="btn btn-primary">OK</button>
      </div>
    </div>
  </form>
     </div>
    </div>
    </div>
  </div>
</main>
@endsection