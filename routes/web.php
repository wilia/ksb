<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Route::get('/satu','CollectionController@collectionSatu');
Route::get('/dua','CollectionController@collectionDua');
Route::get('/tiga','CollectionController@collectionTiga');
Route::get('/empat','CollectionController@collectionEmpat');
Route::get('/lima','CollectionController@collectionLima');
Route::get('/enam','CollectionController@collectionEnam');

Route::get('/cek-object','MahasiswaConrtoller@cekObject');

Route::get('/insert','MahasiswaConrtoller@insert');

Route::get('/update','MahasiswaConrtoller@update');
Route::get('/update-where','MahasiswaConrtoller@updateWhere');

Route::get('/delete','MahasiswaConrtoller@delete');

Route::get('/all','MahasiswaConrtoller@all');
Route::get('/all-view','MahasiswaConrtoller@allView');
Route::get('/get-where','MahasiswaConrtoller@getWhere');
Route::get('/test-where','MahasiswaConrtoller@testWhere');
Route::get('/first','MahasiswaConrtoller@first');
Route::get('/find','MahasiswaConrtoller@find');
Route::get('/latest','MahasiswaConrtoller@latest');
Route::get('/limit','MahasiswaConrtoller@limiit');

Route::get('/mass-assignment','MahasiswaConrtoller@massAssignment');
Route::get('/mass-update','MahasiswaConrtoller@massUpdate');

//barang
Route::get('/barangs','BarangController@index')->name('barangs.index');
Route::get('/barangs/create','BarangController@create')->name('barangs.create');
Route::post('/barangs','BarangController@store')->name('barangs.store');
Route::get('/barangs/{barang}','BarangController@detail')->name('barangs.detail');
Route::get('/barangs/{barang}/edit','BarangController@edit')->name('barangs.edit');
Route::put('/barangs/{barang}', 'BarangController@update')->name('barangs.update');
Route::delete('/barangs/{barang}', 'BarangController@delete')->name('barangs.delete');

//CRUD Mahasiswa
Route::get('/mahasiswas','MahasiswaConrtoller@index')->name('mahasiswas.index')->middleware('auth');
Route::get('/mahasiswas/create','MahasiswaConrtoller@create')->name('mahasiswas.create')->middleware('auth');
Route::post('/mahasiswas','MahasiswaConrtoller@store')->name('mahasiswas.store');
Route::get('/mahasiswas/{mahasiswa}','MahasiswaConrtoller@detail')->name('mahasiswas.detail')->middleware('auth');
Route::get('/mahasiswas/{mahasiswa}/edit','MahasiswaConrtoller@edit')->name('mahasiswas.edit')->middleware('auth');
Route::put('/mahasiswas/{mahasiswa}', 'MahasiswaConrtoller@update')->name('mahasiswas.update');
Route::delete('/mahasiswas/{mahasiswa}', 'MahasiswaConrtoller@delete')->name('mahasiswas.delete')->middleware('auth');

//Admin Katalog
Route::get('adminkatalogs', 'AdminKatalogController@index')->name('adminkatalogs.index');
Route::get('adminkatalogs/index', 'AdminKatalogController@index')->name('adminkatalogs.index');
Route::get('adminkatalogs/layanandiajukan', 'AjukanlayananController@pengajuanlayananAdminK')->name('adminkatalogs.layanandiajukan');
Route::get('adminkatalogs/layanantersedia', 'AdminKatalogController@layanantersedia')->name('adminkatalogs.layanantersedia');
Route::get('adminkatalogs/forminput', 'AdminKatalogController@forminput')->name('adminkatalogs.forminput');
Route::get('adminkatalogs/formedit/{id}', 'AdminKatalogController@formedit')->name('adminkatalogs.formedit');
Route::get('adminkatalogs/pratinjau/{layanan_id}', 'AdminKatalogController@pratinjau')->name('adminkatalogs.pratinjau');
Route::get('adminkatalogs/metodelayanan', 'MetodelayananController@metodelayanan')->name('adminkatalogs.metodelayanan');
Route::get('adminkatalogs/tambahmetode', 'AdminKatalogController@tambahmetode')->name('adminkatalogs.tambahmetode');
Route::get('adminkatalogs/editmetode/{id}', 'MetodelayananController@editmetode')->name('adminkatalogs.editmetode');
Route::post('/buatLayanan', 'AdminKatalogController@buatLayanan')->name('buatLayanan');
Route::post('/ubahLayanan/{id}', 'AdminKatalogController@ubahLayanan')->name('ubahLayanan');
Route::get('adminkatalogs/hapusLayanan/{id}', 'AdminKatalogController@hapusLayanan')->name('hapusLayanan');
Route::get('adminkatalogs/detailAdminKatalog/{id}', 'AdminKatalogController@detailAdminKatalog')->name('admins.detailAdminKatalog');


//Admin Akun
Route::get('admins', 'AdminController@index')->name('admins.index');
Route::get('admins/index', 'AdminController@index')->name('admins.index');
Route::get('admins/akunrequester', 'AdminController@akunrequester')->name('admins.akunrequester');
Route::get('admins/pengajuanakun', 'AjukanakunController@pengajuanakun')->name('admins.pengajuanakun');
Route::get('admins/buatakun/{id}', 'AdminController@buatakun')->name('admins.buatakun');
Route::get('admins/delete/{id}', 'AdminController@delete');
Route::get('admins/restore/{id}', 'AdminController@restore');
Route::get('admins/detailAdmin/{id}', 'AdminController@detailAdmin')->name('admins.detailAdmin');
Route::post('/createAkun/{id}', 'AdminController@createAkun')->name('createAkun');


//Requester
Route::get('requesters/index', 'RequesterController@index')->name('requesters.index');
Route::get('requesters/layanantersediar', 'RequesterController@layanantersediar')->name('requesters.layanantersediar');
Route::get('requesters/layanandiajukanr', 'AjukanlayananController@pengajuanlayanan')->name('requesters.layanandiajukanr');
Route::get('requesters/formeditpengajuan/{id}', 'AjukanlayananController@formeditpengajuan')->name('requesters.formeditpengajuan');
Route::get('requesters/create', 'RequesterController@create')->name('requesters.create');
Route::get('requesters/pratinjaur', 'RequesterController@pratinjaur')->name('requesters.pratinjaur');
Route::get('ajukanakun', 'RequesterController@ajukanakun')->name('ajukanakun');
Route::get('requesters/detailRequester/{id}', 'RequesterController@detailRequester')->name('requesters.detailRequester');
Route::get('requesters/pratinjau/{layanan_id}', 'AdminKatalogController@pratinjau')->name('adminkatalogs.pratinjau');

//Ajukan Akun
Route::post('/create', 'AjukanakunController@create')->name('create');
Route::get('/tolakAkun/{id}', 'AjukanakunController@tolakAkun');

//Ajukan Layanan
Route::post('/createLayanan', 'AjukanlayananController@createLayanan')->name('createLayanan');
Route::post('/updateLayanan/{id}', 'AjukanlayananController@updateLayanan')->name('updateLayanan');

Auth::routes();

//Metode Layanan
Route::post('/createMetode', 'MetodelayananController@createMetode')->name('createMetode');
Route::post('/updateMetode/{id}', 'MetodelayananController@updateMetode')->name('updateMetode');
Route::get('adminkatalogs/hapusMetode/{id}', 'MetodelayananController@hapusMetode')->name('hapusMetode');

//Layanan
Route::get('/tampilJson', 'LayananController@tampilJson')->name('tampilJson');
Route::get('adminkatalogs/formparameter/{id}', 'ParameterController@formparameter')->name('adminkatalogs.formparameter');
Route::post('/createParameter', 'ParameterController@createParameter')->name('createParameter');
Route::get('adminkatalogs/formparameter/deleteParameter/{id}', 'ParameterController@deleteParameter');


Route::get('/home', 'HomeController@home')->name('home');