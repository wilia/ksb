<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parameter extends Model
{
    protected $table = 'ksb_parameter';

    protected $fillable = [
        'id','id_layanan','nama_layanan','parameter','created_date','created_by','modified_date','modified_by','deleted_date','deleted_by'
    ];

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'modified_date';
}
