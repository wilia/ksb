<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ksb_layanan extends Model
{
    protected $table = 'ksb_layanans';

    protected $fillable = [
        'id','nama_pemesan','instansi','nama_layanan','url_layanan','parameter','query_layanan','batas_mulai','batas_selesai','status','created_date','created_by','modified_date','modified_by','deleted_date','deleted_by','active'
    ];

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'modified_date';
}
