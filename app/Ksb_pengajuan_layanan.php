<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ksb_pengajuan_layanan extends Model
{
    protected $table = "ksb_pengajuan_layanans";
    protected $fillable = [
        'id','nama_pemesan','instansi','nama_layanan','alamat_ip','acces_key','keterangan','status_pengajuan','created_date','created_by','modified_date','modified_by','deleted_date','deleted_by','created_at','updated_at'
    ];

}
