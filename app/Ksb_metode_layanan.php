<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ksb_metode_layanan extends Model
{
    protected $table = 'ksb_metode_layanans';

    protected $fillable = [
        'id','nama_instansi','nama_layanan','nama_metode','jenis_metode','status','created_date','created_by','modified_date','modified_by','deleted_date','deleted_by','created_at','updated_at','active'
    ];
}
