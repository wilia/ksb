<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ksb_pengajuan_akun extends Model

{
    protected $table = 'ksb_pengajuan_akuns';
  //  protected $guarded = [];
    
    protected $fillable = [
        'id','nip','nama_lengkap','user_role_grup_id','user_name','user_password','user_email','instansi','suratTugas','ktp','keterangan','status','created_date','created_by','modified_date','modified_by','deleted_date','deleted_by','created_at','updated_at','active'
    ];

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'modified_date';
    
}
