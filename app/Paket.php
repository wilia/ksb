<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paket extends Model
{
    protected $table = 'paket';

    protected $fillable = [
        'id','komoditas_id','penyedia_id','penyedia_distributor_id','rup_id','no_paket','provinsi_id',
        'kabupaten_id','nama_paket','sumber_dana_id','satuan_kerja_nama','satuan_kerja_alamat',
        'satuan_kerja_npwp','kode_anggaran','panitia_user_id','panitia_email','panitia_no_telp',
        'panitia_jabatan','ppk_user_id','ppk_jabatan','ppk_nip','status_progress','apakah_batal',
        'alasan_batal','tanggal_batal','dibatalkan_oleh','deskripsi','kurs_id_from','kurs_nilai',
        'kurs_tanggal','sudah_dikirim','sudah_diterima','sudah_diterima_semua','apakah_selesai',
        'tanggal_selesai','diselesaikan_oleh','sudah_dirating','rating_nilai_avg',
        'rating_nilai_lama_respon','rating_nilai_pelayanan','rating_nilai_akurasi',
        'rating_nilai_kecepatan','rating_komentar','agr_paket_produk','agr_paket_kontrak',
        'agr_paket_pembayaran','agr_paket_pembayaran_total_invoice','agr_konversi_harga_total',
        'agr_paket_produk_sudah_diterima_semua','active','created_date','created_by','modified_date',
        'modified_by','deleted_date','deleted_by','panitia_nip','ppk_email','ppk_telepon','satker_id',
        'kldi_jenis','kldi_id','tahun_anggaran','ppk_sertifikat','pp_sertifikat','instansi_jenis_id',
        'instansi_id','satuan_kerja_kabupaten','pengiriman_alamat','pengiriman_kabupaten',
        'agr_total_harga_diterima_dan_ongkir','penyedia_lama_respon','check_tkdn','catatan_check_tkdn',
        'ppk_sudah_dirating','rating_kecepatan_bayar','rating_penerbitan_surat','rating_responsif',
        'rating_komentar_penyedia','rating_nilai_ppk_avg',
    ];
}

