<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\User;
use Auth;
use Alert;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    
    public function login(Request $request)
    {
       
        
        $user = User::where('user_email', $request->email)
                    ->where('user_password',md5($request->password))
                    ->where('active',true)
                    ->first();

        if(empty($user)){
            return redirect()->back()->with('flash-error','Username atau Password tidak sesuai.');
        }else{
            Auth::login($user);

            // jika user login admin master
            if($user->user_role_grup_id == 1){

                // echo '<pre>';
                //     print_r('login sebagai admin');
                // echo '</pre>';
                // exit();

                return redirect('/admins/index');

            // jika user login PPK/Pejabat Pembuat Komitmen
            }elseif ($user->user_role_grup_id == 4) {
                
                // echo '<pre>';
                //     print_r('login sebagai PPK');
                // echo '</pre>';
                // exit();

                return redirect('/adminkatalogs/index');
            
            // jika selain yang diatas
            }else{
                return redirect('/requesters/index');
            }
            
        }

    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
   protected $redirectTo  = '/admins/index';
    
//    if (Auth::users()->level=="admin"){
//        return view('/admins/index',$data);
//    }

//     elseif(Auth::users()->level=="requester"){
//         return view('/requesters/index',$data);
//     }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
