<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CollectionController extends Controller
{
    public function collectionSatu()
    {
        $myArray=[1,3,5,7,8,9];
        $collection= collect($myArray);
        
        echo $collection[0] ."<br>";
        echo $collection[3] ."<br>";

        foreach ($collection as $value){
            echo $value ." ";
        }

    }

    public function collectionDua()
    {
        $myArray=["Belajar","collection","laravel",6];
        $collection=collect($myArray);

        dump($collection);

        foreach ($collection as $value) {
            echo $value. "<br>";
        }

        $myArray2=["nama"=>"Wilia","umur"=>21];
        $collection=collect($myArray2);

        dump($collection);

        foreach ($collection as $key => $value) {
            echo $key. "= ".$value. "<br>";
        }
    }

    public function collectionTiga()
    {
       // $collection = collect([1,9,3,4,5,3,5,7]);

      //  dump($collection->sum());
        //dump($collection->avg());
        //dump($collection->max());
        //dump($collection->min());
        //dump($collection->median());

        //dump ( $collection->random() );
        //echo $collection->concat([10,11,12]);
        //dump( $collection->contains(3) );

        //$collection = collect(["0"=>1,"1"=>9,"2"=>3,"4"=>5,"5"=>3,"6"=>5,]);
        //$collection->unique();
        //dump($collection->all() );

        $varA = ([1,2,3]);
        $varB = collect([1,2,3]);
        
        dump($varA);
        dump($varB);
    }
    
    public function collectionEmpat()
    {
        $collection=collect([1, 2, 3, 4]);
        
        echo "Menggunakan first() <br>";
        dump($collection->first()); // 1

        echo "Menggunakan last() <br>";
        dump($collection->last());

        echo "Menggunakan count() <br>";
        dump($collection->count());

        echo "Menggunakan sort() <br>";
        dump($collection->sort());

        echo "Menggunakan get () <br>";
        $collection = collect(['nama' => 'Wilia Putri Noor', 'framework' => 'laravel']);
        $value = $collection->get('nama');
        echo ($collection->get('nama'))."<br>"; 

        "<br>";
        echo "Menggunakan has() <br>";
        $collection = collect(['account_id' => 1, 'product' => 'Desk', 'amount' => 5]);
        dump ($collection->has('product')); 
        dump ($collection->has(['product', 'amount']));
        dump ($collection->has(['amount', 'price']));

        echo "Menggunakan replaced() <br>";
        $collection = collect(['Wilia', 'Putri', 'Noor']);
        $replaced = $collection->replace([1 => 'bunga', 3 => 'kembang']);
        dump ($replaced->all()); 

        echo "Menggunakan forget()<br>";
        $collection = collect(['name' => 'taylor', 'framework' => 'laravel']);
        $collection->forget('name');
        dump ($collection->all());

    }

    public function collectionLima(){
        $collection = collect ([
            "Nama"=> "Wilia Putri Noor",
            "NIM" => "J3C118128",
            "Universitas" => "Sekolah Vokasi IPB",
            "jurusan" => "Manajemen Informatika",
        ]);

        //( $collection->keys() );
        //dump( $collection->values() );
        
        //method each sebagai bentuk lain dari foreach
        $collection->each(function ($val,$key){
            echo "$key: $val <br>";
        });
    }
    
    public function collectionEnam()
    {
        $collection = collect([
            ['namaProduk' => 'Gamis A', 'harga' => 239990],
            ['namaProduk' => 'Sarung B', 'harga' => 129990],
            ['namaProduk' => 'Mukena C', 'harga' => 450000],
        ]);

        dump($collection);

        //TUGAS 2 
        
        echo "Fungsi urutkan berdasarkan key harga <br>";
        dump( $collection->sortBy('harga') );

        echo "Urutkan berdasarkan key harga <br>";
        dump( $collection->sortByDesc('harga') );
        
        echo "Urutkan berdasarkan key harga dan tampilkan sebagai array <br>";
        dump( $collection->sortBy('harga')->all() );

        echo "Urutkan berdasarkan key harga dan tampilkan dengan method each <br>";
        $collection->sortBy('harga')->each(function($val, $key) {
            echo $val['namaProduk']."<br>";
        });

        echo "Filter untuk mengambil element collection dengan harga < 200000 <br>";
        $hasil = $collection->filter(function($val, $key){
            return $val['harga'] < 200000;
        });

        dump( $hasil );

        echo "Cari element yang key harga bernilai 450000 <br>";
        dump( $collection->where('harga', 450000) );

        echo "Tampilkan nama produk yang element key harga lebih dari 150000 <br>";
        dump( $collection->where('harga','>=', 150000) );

        echo "Tampilkan nama produk yang element key harga bernilai 450000 <br>";
        $hasil = $collection->where('harga', 450000)->first();
        echo $hasil['namaProduk']."<br>";

        echo "Tampilkan nama produk yang element key harga bernilai 450000 <br>";
        $hasil = $collection->firstWhere('harga', 450000);
        echo $hasil['namaProduk']."<br>";

        echo "Tampilkan nama produk yang element key harga lebih dari 150000 <br>";
        $hasil = $collection->where('harga','>=', 150000)->all();

        echo "Cari element dengan harga antara 150000 - 500000  <br>";
            dump( $collection->WhereNotBetween('harga',[150000, 500000]) );

        echo "Cari element dengan harga  129990, 239990, atau 350000 <br>";
            dump( $collection->WhereIn('harga',[129990, 239990, 350000]) );

        echo "Cari element dengan harga selain 129990, 239990, 350000 <br>";
            dump( $collection->whereNotIn('harga',[129990, 239990, 350000]) );
    }
}
