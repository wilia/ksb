<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Parameter;
use App\Ksb_layanan;

class ParameterController extends Controller
{
    public function formparameter($layanan_id)
    {
        $ksb_parameters = Parameter::whereIdLayanan($layanan_id)->get();
        $ksb_layanan = Ksb_layanan::whereId($layanan_id)->first();

        // return view('adminkatalog.formparameter',['ksb_layanan' => $ksb_layanan]);
        return view('adminkatalog.formparameter',compact('ksb_parameters', 'ksb_layanan'));
    }

    public function createParameter(Request $request)
    {
        //dd($request->all());
        Parameter::create([
            'id_layanan' => $request->id_layanan,
            'nama_layanan' => $request->nama_layanan,
            'parameter' => $request->parameter,
        ]);

        return redirect('adminkatalogs/layanantersedia');
    }

    public function deleteParameter($id){

        // print_r($id);
        // exit();

        // $ksb_parameters = Parameter::find($id);
        // $ksb_parameters->deleted_date = date('Y-m-d H:i:s');
        // $ksb_parameters->active = 0;
        // $ksb_parameters->save();
        $ksb_parameters = Parameter::find($id);
        $ksb_parameters->delete();
        return Redirect::action('AdminKatalogController@layanantersedia');
        
    }
}
