<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Ksb_pengajuan_akun;

class AjukanakunController extends Controller
{
    public function create(Request $request)
    {
        //dd($request->all());
        Ksb_pengajuan_akun::create([
            'user_role_grup_id' => $request->user_role_grup_id,
            'nip' => $request->nip,
            'nama_lengkap' => $request->nama_lengkap,
            'user_name' => $request->user_name,
            'user_email' => $request->user_email,
            // 'user_password' => $request->user_password,
            'user_password' => md5($request->user_password),
            'instansi' => $request->instansi,
            'suratTugas' => $request->suratTugas,
            'ktp' => $request->ktp,

      ]);

        return redirect('/');
    }

    public function pengajuanakun()
    {
        $ksb_pengajuan_akuns = Ksb_pengajuan_akun::paginate(10);

        // contoh return xml
        //return response()->xml($ksb_pengajuan_akuns);
        
        // contoh return json
        //return response()->json($ksb_pengajuan_akuns);
        
        return view('admin.pengajuanakun', compact('ksb_pengajuan_akuns'));
    }

    public function tolakAkun($id){

        // print_r($id);
        // exit();

        $ksb_pengajuan_akuns = Ksb_pengajuan_akun::find($id);
        $ksb_pengajuan_akuns->deleted_date = date('Y-m-d H:i:s');
        $ksb_pengajuan_akuns->active = 1;
        $ksb_pengajuan_akuns->save();
        //return Redirect::action('AdminController@akunrequester')->with('sukses','Pengguna Berhasil dinonaktifkan');
        return Redirect::action('AjukanakunController@pengajuanakun');
        
    }

}   
