<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ksb_pengajuan_layanan;
use App\Ksb_layanan;
use App\Ksb_metode_layanan;
use App\User;

class RequesterController extends Controller
{
    public function index()
    {
        return view('requester.index');
    }

    public function layanantersediar()
    {
        $ksb_layanans = Ksb_layanan::paginate(10);
        //return view('adminkatalog.layanantersedia',['ksb_layanans' => $ksb_layanans]);
        return view('requester.layanantersediar',['ksb_layanans' => $ksb_layanans]);
    }

    // public function layanandiajukanr()
    // {
    //     return view('requester.layanandiajukanr');
    // }

    // public function formeditpengajuan()
    // {
    //     return view('requester.formeditpengajuan');
    // }

    // public function formajukanlayanan()
    // {
    //     return view('requester.formajukanlayanan');
    // }

    public function pratinjaur()
    {
        return view('requester.pratinjaur');
    }

    public function ajukanakun()
    {
        return view('ajukanakun');
    }

    public function create(){
        $metod_layan = Ksb_metode_layanan::get();
        // echo '<pre>';
        // print_r($metod_layan);
        // echo '</pre>';
        // exit();
       // return view('adminkatalog.forminput',compact('metod_layan'));
        return view('requester.formajukanlayanan',compact('metod_layan'));
    }

    public function detailRequester($id)
    { 
        $user = User::find($id);
        return view('requester.detailRequester',['user' => $user]);
    }
    
}
