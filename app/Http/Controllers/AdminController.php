<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\User;
use App\Ksb_pengajuan_akun;
use Auth;

class AdminController extends Controller
{
    
    public function index()
    {
        $user = Auth::user();

        // echo '<pre>';
        //     print_r($user->nama_lengkap);
        // echo '</pre>';
        // exit();

        return view('admin.index', compact('user'));
    }

    public function akunrequester(Request $request)
    { 

        $users = User::paginate(10);

        if($request->has('cari')){
            $users = \App\User::where('nama_lengkap','LIKE','%'.$request->cari.'%')->get();
        }else{
            $users = User::paginate(10);
        }
       
        return view('admin.akunrequester',['users' => $users]);

    }

    // public function pengajuanakun()
    // {
    //     $ksb_pengajuan_akun = Ksb_pengajuan_akun::paginate(10);
    //     return view('admin.pengajuanakun');
    // }

    public function buatakun($id)
    { 
        $ksb_pengajuan_akuns = Ksb_pengajuan_akun::findorfail($id);

        // insert tabel user
        // user->nama = ksb_pengajuan_akuns->nama;
        // user->password = ksb_pengajuan_akuns->password;


        // return view('adminkatalog.editmetode', compact('ksb_metode_layanans'));
        return view('admin.buatakun',compact('ksb_pengajuan_akuns'));
        // return redirect()->back()->with('flash-error','Username atau Password tidak sesuai.');
    }

    public function delete($id){

        // print_r($id);
        // exit();

        $user = User::find($id);
        $user->deleted_date = date('Y-m-d H:i:s');
        $user->active = 0;
        $user->save();
        return Redirect::action('AdminController@akunrequester')->with('sukses','Pengguna Berhasil dinonaktifkan');
        
    }

    public function restore($id){
        $user = User::find($id);
        $user->deleted_date = null;
        $user->active = 1;
        $user->save();

        return Redirect::action('AdminController@akunrequester')->with('sukses','Pengguna Berhasil diaktifkan');
    }

    public function detailAdmin($id)
    { 
        $user = User::find($id);
        return view('admin.detailAdmin',['user' => $user]);
    }

    public function createAkun(Request $request, $id)
    {
        // dd($request->all());
        User ::create([
            'user_role_grup_id' => $request->user_role_grup_id,
            'nama_lengkap' => $request->nama_lengkap,
            'user_name' => $request->user_name,
            'user_email' => $request->user_email,
            'user_password' => $request->user_password,
            'nip' => $request->nip,
            'status' => $request->status,
            'created_by' => Auth::user()->id,
            
        ]);

        return redirect('admins/akunrequester');
    }
}
