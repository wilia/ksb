<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mahasiswa;

class MahasiswaConrtoller extends Controller
{
    //cek object mahasiswa
    public function cekObject()
    {
        $mahasiswa= new Mahasiswa;

        dump($mahasiswa);
    }
    public function insert(){
        $mahasiswa = new Mahasiswa;
        $mahasiswa->nim = 'G64104071';
        $mahasiswa->nama = 'Ringga Gilang Baskoro';
        $mahasiswa->tempat_lahir = 'klaten';
        $mahasiswa->tanggal_lahir = '1986-11-20';
        $mahasiswa->fakultas = 'MIPA';
        $mahasiswa->jurusan = 'Ilmu Komputer';
        $mahasiswa->ipk = 3.5;
        $mahasiswa->save();

        dump($mahasiswa);
    }
    public function massAssignment(){
        $mahasiswa1=Mahasiswa::create(
            [
                'nim' => 'J3C123457',
                'nama' => 'Rudi Permana',
                'tempat_lahir' => 'Bogor',
                'tanggal_lahir' => '2000-08-22',
                'fakultas' => 'Vokasi',
                'jurusan' => 'Manajemen Informatika',
                'ipk' => 3.5,
            ]
        );

        $mahasiswa2=Mahasiswa::create(
            [
                'nim' => 'J3C118128',
                'nama' => 'Wilia Putri Noor',
                'tempat_lahir' => 'Bandung',
                'tanggal_lahir' => '1999-01-03',
                'fakultas' => 'Vokasi',
                'jurusan' => 'Manajemen Informatika',
                'ipk' => 3.8,
            ]
        );

        $mahasiswa3=Mahasiswa::create(
            [
                'nim' => 'J3D118009',
                'nama' => 'Andi',
                'tempat_lahir' => 'Depok',
                'tanggal_lahir' => '2000-08-10',
                'fakultas' => 'Vokasi',
                'jurusan' => 'Teknik Komputer',
                'ipk' => 3.2,
            ]
        );

        $mahasiswa4=Mahasiswa::create(
            [
                'nim' => 'J3A118189',
                'nama' => 'Hasnah',
                'tempat_lahir' => 'Bogor',
                'tanggal_lahir' => '2000-09-09',
                'fakultas' => 'Vokasi',
                'jurusan' => 'Komunikasi',
                'ipk' => 3.7,
            ]
        );

        $mahasiswa1=Mahasiswa::create(
            [
                'nim' => 'J3C118100',
                'nama' => 'Dono',
                'tempat_lahir' => 'Bogor',
                'tanggal_lahir' => '2000-11-22',
                'fakultas' => 'Vokasi',
                'jurusan' => 'Manajemen Informatika',
                'ipk' => 3.0,
            ]
        );
        return "Berhasil di proses";
    }
   // public function update(){
     //   $mahasiswa = Mahasiswa::find(2);
       // $mahasiswa->tempat_lahir = 'Semarang';
        //$mahasiswa->save();

       // dump($mahasiswa);
   // }
    public function updateWhere(){
        $mahasiswa = Mahasiswa::where('nim','J3C123456')->first();
        $mahasiswa->ipk = 4.0;
        $mahasiswa->save();

        dump($mahasiswa);
    }
    public function massUpdate(){
        Mahasiswa::where('nim','G64104071')->first()->update([
            'tanggal_lahir' =>'1999-11-20',
            'ipk' => 3.0
        ]);
        return "berhasil di update";
    }
   // public function delete(){
     //   $mahasiswa = Mahasiswa::where('ipk','>',3)->delete();
       // dump($mahasiswa);
    //}
    public function all(){
        $result = Mahasiswa::all();
        foreach ($result as $mahasiswa){
            echo($mahasiswa->id). '<br>';
            echo($mahasiswa->nim). '<br>';
            echo($mahasiswa->nama). '<br>';
            echo($mahasiswa->tempat_lahir). '<br>';
            echo($mahasiswa->tanggal_lahir). '<br>';
            echo($mahasiswa->fakultas). '<br>';
            echo($mahasiswa->jurusan). '<br>';
            echo($mahasiswa->ipk). '<br>';
            echo "<hr>";
        }
    }
    public function allView(){
        $mahasiswas = Mahasiswa::all();
        return view('tampil-mahasiswa',['mahasiswas' => $mahasiswas]);
    }
    public function getWhere(){
        $mahasiswas = Mahasiswa::where('ipk','>','3')
        ->orderBy('nama','desc')
        ->get();
        return view('tampil-mahasiswa',['mahasiswas' => $mahasiswas]);
    }
    //index
    public function index(){
        $mahasiswas = Mahasiswa::all();
        return view('mahasiswa.index',['mahasiswas' => $mahasiswas]);
    }
    //fungsi create
    public function create(){
        return view('mahasiswa.form-mahasiswa');
    }
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'nim' => 'required|size:9|unique:mahasiswas',
            'nama' => '',
            'tempat_lahir' => '',
            'tanggal_lahir' => '',
            'fakultas' => '',
            'jurusan' => '',
            'ipk' => '',

        ]);
        Mahasiswa::create($validateData);

        $request->session()->flash('pesan',"Data berhasil ditambahkan");
        return redirect()->route('mahasiswas.index');
    }
    public function detail($mahasiswa)
    {
        $result = Mahasiswa::find($mahasiswa);
        return view('mahasiswa.detail-mahasiswa',['mahasiswa'=>$result]);
    }
    public function edit($mahasiswa)
    {
        $result=Mahasiswa::find($mahasiswa);
        return view('mahasiswa.edit-mahasiswa',['mahasiswa'=>$result]);
    }
    public function update(Request $request, Mahasiswa $mahasiswa)
    {
        $validateData = $request->validate([
            'nim' => 'required|size:9|unique:mahasiswas,nim,'.$mahasiswa->id,
            'nama' => '',
            'tempat_lahir' => '',
            'tanggal_lahir' => '',
            'fakultas' => '',
            'jurusan' => '',
            'ipk' => '',
        ]);

        Mahasiswa::where('id',$mahasiswa->id)->update($validateData);
        $request->session()->flash('pesan',"Data berhasil diperbarui");
        return redirect()->route('mahasiswas.detail',['mahasiswa'=>$mahasiswa->id]);

    }
    public function delete(Mahasiswa $mahasiswa)
    {
        $mahasiswa->delete();
        return redirect()->route('mahasiswas.index')
        ->with('pesan', "Data berhasil dihapus");
    }
}
