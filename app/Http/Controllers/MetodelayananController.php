<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ksb_metode_layanan;

class MetodelayananController extends Controller
{
    public function createMetode(Request $request)
    {
       // dd($request->all());
        Ksb_metode_layanan::create([
            'nama_instansi' => $request->nama_instansi,
            'nama_layanan' => $request->nama_layanan,
            'nama_metode' => $request->nama_metode,
            'jenis_metode' => $request->jenis_metode,
            'status' => $request->status,
        ]);

        return redirect('adminkatalogs/metodelayanan');
    }

    public function metodelayanan()
    {
        $ksb_metode_layanans = Ksb_metode_layanan::paginate(10);
        return view('adminkatalog.metodelayanan',['ksb_metode_layanans' => $ksb_metode_layanans]);
    }

    public function editmetode($id)
    {
        $ksb_metode_layanans = Ksb_metode_layanan::findorfail($id);
        return view('adminkatalog.editmetode', compact('ksb_metode_layanans'));
    }

    public function updateMetode(Request $request, $id)
    {
        $ksb_metode_layanans = Ksb_metode_layanan::findorfail($id);
        $ksb_metode_layanans ->update($request->all());
        return redirect('adminkatalogs/metodelayanan');
    }

    public function hapusMetode(Request $request, $id)
    {
        $ksb_metode_layanans = Ksb_metode_layanan::findorfail($id);
        $ksb_metode_layanans ->delete();
        return back();
    }
}
