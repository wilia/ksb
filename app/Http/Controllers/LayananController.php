<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Paket;
use DB;

class LayananController extends Controller
{
    public function tampilJson(Request $request)
    {
        $where = '';

        if(!empty($request->id_kldi)){
            $where .= " and p.kldi_id = '".$request->id_kldi."'";
        }
        
        if(!empty($request->kldiId)){
            $where .= " and p.kldi_id = '".$request->kldiId."'";
        }
        
        if(!empty($request->tahun)){
            $where .= ' and YEAR(p.created_date) = '.$request->tahun;
        }
        
        if(!empty($request->no_paket)){
            $where .= " and p.no_paket = '".$request->no_paket."'";
        }

         $paket = DB::select(DB::raw(" SELECT
                                p.no_paket,
                                p.rup_id,
                                p.tahun_anggaran,
                                k.nama_komoditas,
                                CASE 
                                when
                                    kk.nama_kategori = 'lokal'
                                    then ('lokal')
                                when
                                    kk.nama_kategori = 'sektoral'
                                    then ('sektoral')
                                else ('nasional')
                                end as jenis_katalog,
                                klkom.nama nama_instansi_katalog,
                                p.nama_paket,
                                kl.jenis jenis_instansi,
                                kl.nama nama_instansi,
                                ks.nama satuan_kerja_nama,
                                p.satuan_kerja_alamat,
                                usrppk.nama_lengkap nama_ppk,
                                pk.nama_kategori,
                                m.nama_manufaktur,
                                prd.id,
                                prd.nama_produk,
                                prd.jenis_produk,
                                pprod.kuantitas,
                                pprod.konversi_harga_satuan harga_satuan,
                                pprod.konversi_harga_ongkir ongkos_kirim,
                                pprod.konversi_harga_total total_harga,
                                p.created_date tanggal_buat_paket,
                                pny.nama_penyedia,
                                pnydis.nama_distributor,
                                ps.deskripsi,
                                ps.status_paket
                                
                            FROM
                                paket p
                            LEFT JOIN komoditas k ON p.komoditas_id = k.id
                            LEFT JOIN komoditas_kategori kk ON k.komoditas_kategori_id = kk.id
                            LEFT JOIN kldi kl ON p.kldi_id = kl.id
                            LEFT JOIN kldi_satker ks ON ks.id = p.satker_id
                            LEFT JOIN kldi klkom ON k.kldi_id = klkom.id
                            LEFT JOIN penyedia pny ON p.penyedia_id = pny.id
                            LEFT JOIN penyedia_distributor pnydis ON p.penyedia_distributor_id = pnydis.id
                            LEFT JOIN `user` usrppk ON p.ppk_user_id = usrppk.id
                            LEFT JOIN paket_produk pprod ON p.id = pprod.paket_id
                            LEFT JOIN produk prd ON pprod.produk_id = prd.id
                            LEFT JOIN manufaktur m ON prd.manufaktur_id = m.id
                            LEFT JOIN produk_kategori pk ON prd.produk_kategori_id = pk.id
                            LEFT JOIN paket_status ps ON p.id = ps.paket_id
                            WHERE
                            p.active = 1
                            $where
                            and pprod.active = 1
                            AND p.apakah_batal = 0
                            AND (
                                        (ps.status_paket IN ('ppk_setuju','proses_kirim','paket_selesai') 
                                        OR
                                        (ps.status_paket = 'penyedia_setuju' and ps.from_status = 'penyedia' and ps.to_status = 'distributor' and (SELECT user_role_grup_id FROM user where id = p.created_by) = 4 )
                                        OR
                                        (ps.status_paket = 'penyedia_setuju' and ps.from_status = 'penyedia' and ps.to_status = 'penyedia' and (SELECT user_role_grup_id FROM user where id = p.created_by) = 4 )
                                        )
                                    )
                            ORDER BY
                                p.created_date;"
        ));
        
        if ($request->type_print == 'xml') {
            return response()->xml($paket);
        }else{
            return response()->json($paket);
        }

    }
}
