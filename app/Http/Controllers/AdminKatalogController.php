<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Ksb_layanan;
use App\Ksb_metode_layanan;
use App\Parameter;
use App\User;

class AdminKatalogController extends Controller
{
    //
    public function index()
    {
        // print_r('cek');
        // exit();

        return view('adminkatalog.index');
    }

    public function layanantersedia()
    {
        $ksb_layanans = Ksb_layanan::paginate(10);
        return view('adminkatalog.layanantersedia',['ksb_layanans' => $ksb_layanans]);
    }

    public function buatLayanan(Request $request)
    {
        //dd($request->all());
        Ksb_layanan::create([
            'nama_pemesan' => $request->nama_pemesan,
            'instansi' => $request->instansi,
            'nama_layanan' => $request->nama_layanan,
            'url_layanan' => $request->url_layanan,
            'parameter' => $request->parameter,
            'query_layanan' => $request->query_layanan,
            'batas_mulai' => $request->batas_mulai,
            'batas_selesai' => $request->batas_selesai,
            'status' => $request->status,
        ]);

        // Parameter::create([
        //     'nama_layanan' => $request->nama_layanan,
        // ]);


        return redirect('adminkatalogs/layanantersedia');
    }

    public function layanandiajukan()
    {

        return view('adminkatalog.layanandiajukan');
    }

    public function forminput()
    {
        $metod_layan = Ksb_metode_layanan::get();
        // echo '<pre>';
        // print_r($metod_layan);
        // echo '</pre>';
        // exit();
        return view('adminkatalog.forminput',compact('metod_layan'));
    }

    public function formedit($id)
    {
        $metod_layan = Ksb_metode_layanan::get();
        $ksb_layanans = Ksb_layanan::findorfail($id);
        return view('adminkatalog.formedit', compact('ksb_layanans','metod_layan'));
    }

    public function ubahLayanan(Request $request, $id)
    {
        $ksb_layanans = Ksb_layanan::findorfail($id);
        $ksb_layanans ->update($request->all());
        return redirect('adminkatalogs/layanantersedia');
    }

    public function hapusLayanan($id){

        // print_r($id);
        // exit();
        $ksb_layanans = Ksb_layanan::find($id);
        $ksb_layanans->delete();
        return Redirect::action('AdminKatalogController@layanantersedia');
        
    }

    public function pratinjau($layanan_id)
    {
        $ksb_parameters = Parameter::whereIdLayanan($layanan_id)->get();

        return view('adminkatalog.pratinjau', compact('ksb_parameters'));
    }

    // public function metodelayanan()
    // {

    //     return view('adminkatalog.metodelayanan');
    // }

    public function tambahmetode()
    {

        return view('adminkatalog.tambahmetode');
    }

    public function detailAdminKatalog($id)
    { 
        $user = User::find($id);
        return view('adminkatalog.detailAdminKatalog',['user' => $user]);
    }

    // public function editmetode()
    // {

    //     return view('adminkatalog.editmetode');
    // }
}
