<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ksb_pengajuan_layanan;

class AjukanlayananController extends Controller
{
    public function createLayanan(Request $request)
    {
        //dd($request->all());
        Ksb_pengajuan_layanan::create([
            'nama_pemesan' => $request->nama_pemesan,
            'instansi' => $request->instansi,
            'nama_layanan' => $request->nama_layanan,
            'alamat_ip' => $request->alamat_ip,
            'acces_key' => $request->acces_key,
            'keterangan' => $request->keterangan,
        ]);

        return redirect('requesters/layanandiajukanr');
    }

    public function pengajuanlayanan()
    {
        
        $ksb_pengajuan_layanans = Ksb_pengajuan_layanan::paginate(10);
        return view('requester.layanandiajukanr',['ksb_pengajuan_layanans' => $ksb_pengajuan_layanans]);
    }

    public function pengajuanlayananAdminK()
    {
        $ksb_pengajuan_layanans = Ksb_pengajuan_layanan::paginate(10);
        return view('adminKatalog.layanandiajukan',['ksb_pengajuan_layanans' => $ksb_pengajuan_layanans]);
    }

    public function formeditpengajuan($id)
    {
        $ksb_pengajuan_layanans = Ksb_pengajuan_layanan::findorfail($id);
        return view('requester.formeditpengajuan',compact('ksb_pengajuan_layanans'));
    }

    public function updateLayanan(Request $request, $id)
    {
        $ksb_pengajuan_layanans = Ksb_pengajuan_layanan::findorfail($id);
        $ksb_pengajuan_layanans ->update($request->all());
        return redirect('requesters/layanandiajukanr');
    }

}
