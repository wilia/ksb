<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function home()
    {
        // print_r('cek');
        // exit();

        return view('home');
    }

    public function index()
    {
        // print_r('cek');
        // exit();

        return view('index');
    }

    public function sidebarleft()
    {
        // print_r('cek');
        // exit();

        return view('sidebarleft');
    }

}
