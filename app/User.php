<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;


class User extends Authenticatable
{
    protected $table = 'user';
    use Notifiable;
    //use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','user_role_grup_id','nama_lengkap','user_name','user_password',
        'user_email','bahasa','role_basic_id','override_role_grup','override_role_komoditas',
        'override_role_paket','akses_seluruh_paket','akses_seluruh_komoditas',
        'status_penyedia_distributor','satuan_kerja_nama','satuan_kerja_alamat',
        'satuan_kerja_npwp','no_telp','nip','jabatan','jenis_instansi_id','instansi_id',
        'param','active','pps_id','created_date','created_by','modified_date','modified_by',
        'deleted_date','deleted_by','nomor_sertifikat_pbj','wilayah_provinsi_id',
        'wilayah_kabupaten_id','satker_id','kldi_jenis','kldi_id','rkn_id','centrum_user_id',
        'centrum_username','tingkat_jabatan','berbatas_waktu','batas_mulai','batas_selesai','durasi'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'modified_date';
}

